<?php

// game loop
while (TRUE)
{
    // $nextCheckpointX: x position of the next check point
    // $nextCheckpointY: y position of the next check point
    // $nextCheckpointDist: distance to the next checkpoint
    // $nextCheckpointAngle: angle between your pod orientation and the direction of the next checkpoint
    fscanf(STDIN, "%d %d %d %d %d %d", $x, $y, $nextCheckpointX, $nextCheckpointY, $nextCheckpointDist, $nextCheckpointAngle);
    fscanf(STDIN, "%d %d", $opponentX, $opponentY);

    // Write an action using echo(). DON'T FORGET THE TRAILING \n
    // To debug: error_log(var_export($var, true)); (equivalent to var_dump)

    // You have to output the target position
    // followed by the power (0 <= thrust <= 100)
    // i.e.: "x y thrust"
    $speed = "100";
    if ($nextCheckpointAngle > 90 || $nextCheckpointAngle < -90){
        $speed = "0";      
    }

    if ($nextCheckpointDist > 5000 && ($nextCheckpointAngle < 30 && $nextCheckpointAngle > -30)) {
        $speed = "BOOST";
    }
    
    echo ($nextCheckpointX . " " . $nextCheckpointY . " " .  $speed . "\n");
}
