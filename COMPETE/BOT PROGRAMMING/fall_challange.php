<?php

while (TRUE)
{
    $game = new game();
    $game->collectGameData();
    $game->collectMakablePotions();
    if ($game->canCreateSomePotion()) {
        debug("createFirstMakablePotion");
        $game->createFirstMakablePotion();    
    } else {
        debug("castSpellForCreateNewIngredients");
        $game->castSpellForCreateNewIngredients();
    }
}

class game {
    public $gameData = [];
    public $makablePotions = [];
    
    // In the first league: BREW <id> | WAIT; later: BREW <id> | CAST <id> [<times>] | LEARN <id> | REST | WAIT
    public $witcherAction = false;

    public function collectGameData() {
        $this->collectActionData();
        $this->collectInventoryData();
    }

    private function collectActionData() {
        // $actionCount: the number of spells and potions in play
        fscanf(STDIN, "%d", $actionCount);

        $this->gameData['potions'] = [];
        $this->gameData['spells'] = [];

        for ($i = 0; $i < $actionCount; $i++)
        {
            // $actionId: the unique ID of this spell or recipe
            // $actionType: in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
            // $delta0: tier-0 ingredient change
            // $delta1: tier-1 ingredient change
            // $delta2: tier-2 ingredient change
            // $delta3: tier-3 ingredient change
            // $price: the price in rupees if this is a potion
            // $tomeIndex: in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
            // $taxCount: in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
            // $castable: in the first league: always 0; later: 1 if this is a castable player spell
            // $repeatable: for the first two leagues: always 0; later: 1 if this is a repeatable player spell
            fscanf(STDIN,"%d %s %d %d %d %d %d %d %d %d %d", 
            $actionId, $actionType, $ingredient_0, 
            $ingredient_1, $ingredient_2, $ingredient_3, $price,
            $tomeIndex, $taxCount, $castable, $repeatable);

            if ($actionType == "BREW" ) {
                $this->gameData['potions'][$actionId] = [
                    "ingredient-0" => $ingredient_0,
                    "ingredient-1" => $ingredient_1,
                    "ingredient-2" => $ingredient_2,
                    "ingredient-3" => $ingredient_3,
                ];

                $this->gameData[$actionType] = 
                $this->gameData['pricePotions'][$actionId] = $price;
            }

            if ($actionType == "CAST" && $castable == 1) {
                $this->gameData['spells'][$actionId] = [
                    "actionId" => $actionId,
                    "actionType" => $actionType,
                    "ingredient-0" => $ingredient_0,
                    "ingredient-1" => $ingredient_1,
                    "ingredient-2" => $ingredient_2,
                    "ingredient-3" => $ingredient_3,
                    "price" => $price,
                    "tomeIndex" => $tomeIndex,
                    "taxCount" => $taxCount,
                    "castable" => $castable,
                    "repeatabl" => $repeatable
                ];    
            }
        }
    }

    private function collectInventoryData() {
        for ($i = 0; $i < 2; $i++)
        {
            // $inv0: tier-0 ingredients in inventory
            // $score: amount of rupees
            fscanf(STDIN, "%d %d %d %d %d", $inv0, $inv1, $inv2, $inv3, $score);
            $this->gameData['inventory'][] = [
                "ingredient-0" => $inv0,
                "ingredient-1" => $inv1,
                "ingredient-2" => $inv2,
                "ingredient-3" => $inv3
            ];
        }
        debug($this->gameData['inventory']);
    }

    public function collectMakablePotions() {
        foreach($this->gameData['potions'] as $potionId => $ingredients) {
            if ($this->hasWitchIngredientsForCreate($ingredients)) {
                $this->makablePotions[$potionId] = $this->gameData['pricePotions'][$potionId]; // / abs(array_sum($potionIngredients));
            }
        }
    }
    
    public function canCreateSomePotion() {
        return (empty($this->makablePotions)) ? false : true;
    }

    public function createFirstMakablePotion() {
        $potionId = array_key_first($this->makablePotions);
        unset($this->makablePotions[$potionId]);
        $this->callWitcherAction("BREW " . $potionId);
    }

    public function castSpellForCreateNewIngredients() {
        // $spells = $this->gameData['spells'];
        // debug($spells);
        
        foreach( $this->gameData['spells'] as $spellId => $spell) {
            if ($this->isNotEnoughIngredientsFromSpell($spell) && $this->isEnoughIngredientsForCastSpell($spell)) {
                $this->callWitcherAction("CAST ". $spellId);
                return;
            }
        }
        $this->callWitcherAction("REST");
    }

    private function isNotEnoughIngredientsFromSpell($spell, $maxLimit = 3) {
        foreach ($this->gameData['inventory'][0] as $key => $countIngredient) {
            if ($spell[$key] > 0) {
                if ($maxLimit-$countIngredient > 0){
                    return true;
                }
            }
        }
        return false;
    }

    private function isEnoughIngredientsForCastSpell($spell) {
        // debug($this->gameData['inventory'][0]);
        foreach ($this->gameData['inventory'][0] as $key => $countIngredient) {
            // debug("spell>".abs($spell[$key]));
            if ($spell[$key] < 0){
                if (($countIngredient - abs($spell[$key])) < 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private function callWitcherAction($action) {
        if ($this->witcherAction){
            $this->debug('You cant set setNewWitcherAction ' . $action. ' 
            When action for actual round is already set :' . $this->witcherAction);
            return;
        }
        echo $action. "\n";
    }

    private function hasWitchIngredientsForCreate($ingredients) {
        $inventory = $this->gameData['inventory'][0];
        foreach ($ingredients as $ingredientType => $ingredientCount) {
            if (abs($ingredientCount) > $inventory[$ingredientType]) {
                return false;
            }
        }
        return true;
    }

    public function debug($var) {
        error_log(var_export($var, true));
    }
}

function debug($var) {
    error_log(var_export($var, true));
}

function getBestRecipeId($potions, $prices, $inventory) {
    $canBeMadePotion = [];
    foreach($potions as $potionId => $potionIngredients){
        if (hasWitchIngredientsForCreatePotion($potionIngredients, $inventory)){
            $canBeMadePotion[$potionId] = $prices[$potionId] / abs(array_sum($potionIngredients));
        }
    }
    asort($canBeMadePotion);
    debug($canBeMadePotion);
    debug("BEST POTION IS ");
    return array_key_last($canBeMadePotion);
}
