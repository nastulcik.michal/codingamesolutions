<?php
// $factoryCount: the number of factories
fscanf(STDIN, "%d", $factoryCount);
// $linkCount: the number of links between factories
fscanf(STDIN, "%d", $linkCount);

// BERE V POTAZ 
// POSILA VZDY NA NEJBLIZSI BUNKU KTERA MA VETSI PRODUKCI NEZ 1 
// POKUD TAKOVA BUNKA NENI POSILA NA POSLEDNI BEZ PRODUKCE
// POSILA BOMBY NA NEPRATELSKE BUNKY KTERE MAJI PRODUKCI 3
// POSILAT Z VICE BUNEK V JEDNOM TAHU
// POSILAT NA VICE BUNEK POKUD JE BUDE MOCT OBSADIT - KONTROLA POCTU POSLANYCH VOJAKU A ZBYVAJICICH
// BRAT V POTAZ UTOKY A ZPROSTRETKOVAT OCHRANU BUNEK - NEPOSILAT Z BUNKY KTERA JE NAPADENA VICE JEDNOTEK NEZ
//  NA NÍ ÚTOČÍ

// VYLEPSENI 
// UDELAT NEJAKE PRAVIDLO PRO CO NEJLEPSI POMER CENA/VYKON - VZDALENOST, POCET VOJAKU, PRODUKCE
// BRAT V POTAZ UTOKY A ZPROSTRETKOVAT OCHRANU BUNEK, NEJLEPE PODLE PRAVIDLA CENA/VYKON

// Collect factory links

$ghostInCell = new GhostInCell($linkCount, $factoryCount);

for ($i = 0; $i < $linkCount; $i++)
{
    fscanf(STDIN, "%d %d %d", $factory1, $factory2, $distance);
    $ghostInCell->setFactoryLink($factory1, $factory2, $distance);
}
$ghostInCell->sortLinksByDistance();

while (TRUE)
{
    $ghostInCell->startTimer();
    $ghostInCell->resetData();

    // $entityCount: the number of entities (e.g. factories and troops)
    fscanf(STDIN, "%d", $entityCount);

    for ($i = 0; $i < $entityCount; $i++) {
        // Entity variables
        fscanf(STDIN,
            "%d %s %d %d %d %d %d", 
            $entityId, $entityType, $arg1, $arg2, $arg3, $arg4, $arg5
        );
        $ghostInCell->collectDataEntity($entityId, $entityType, $arg1, $arg2, $arg3, $arg4, $arg5);
    }

    foreach($ghostInCell->factories['mine'] as $factoryId => $factory) {
        // $ghostInCell->errorLog("TEST : " . $factoryId);
        $ghostInCell->attack($factory);
    }


    $ghostInCell->showTime();
    $ghostInCell->executeCommands();

    // $ghostInCell->errorLog($ghostInCell->factories);
    // echo "WAIT\n";
}


class GhostInCell {
    public $time = 0;

    // Multidimensional array with all routes beetween factory
    public $factoryLinks = [];
    public $factories = [];
    public $troops = [];
    public $bombs = [];

    public $existEnemyFactory = false;
    public $attacks = [];
    public $command = "";
    public $bomb = [
        'send_troop_after_bomb' => false,
        'destination' => false,
        'last_destination' => false,
        'sending' => false,
        'count' => 0,
    ];

    function __construct($factoryCount, $linkCount)
    {
        
        $this->factoryCount = $factoryCount;
        $this->linkCount = $linkCount;
    }

    public function executeCommands() {
        $this->errorLog(strlen($this->command));
        // $this->errorLog($this->factories);

        echo (($this->command) ? rtrim($this->command, "; ") : "WAIT") . "\n"; 
    }

    public function setFactoryLink ($factory1, $factory2, $distance) {
        $this->factoryLinks[$factory1][$factory2] = $distance;
        $this->factoryLinks[$factory2][$factory1] = $distance;
    }

    // Sort links by distance
    public function sortLinksByDistance()
    {
        foreach($this->factoryLinks as $key => $value){
            asort($this->factoryLinks[$key]);
        }
    }

    public function resetData()
    {
        $this->factories = []; $this->troop = []; $this->bombs = []; $this->command = ''; $this->attacks = [];
    }

    public function collectDataEntity($entityId, $entityType, $arg1, $arg2, $arg3, $arg4, $arg5) {
        // arg1: player that owns the factory: 1 for you, -1 for your opponent and 0 if neutral
        // arg2: number of cyborgs in the factory
        // arg3: factory production (between 0 and 3)
        // arg4: number of turns before the factory starts producing again (0 means that the factory produces normally)
        // arg5: unused
        if ($entityType == "FACTORY") {
            $factory = [
                'entityId' => $entityId,
                'owner' => $arg1,
                'cybNumber' => $arg2,
                'production' => $arg3,
                'recovery' => $arg4,
                'arg5' => $arg5,
                'links' => $this->factoryLinks[$entityId],
            ];

            switch ($factory['owner']) {
                case 1:
                    $type = "mine";
                    break;
                case 0:
                    $type = "neutral";
                    break;
                default:
                    $type = "enemy";
                    break;
            }
            $this->factories[$type][$entityId] = $factory;
        }

        // arg1: player that owns the troop: 1 for you or -1 for your opponent
        // arg2: identifier of the factory from where the troop leaves
        // arg3: identifier of the factory targeted by the troop
        // arg4: number of cyborgs in the troop (positive integer)
        // arg5: remaining number of turns before the troop arrives (positive integer)
        if ($entityType == "TROOP") {
            $troop = [
                'entityId' => $entityId,
                'owner' => $arg1,
                'source' => $arg2,
                'destination' => $arg3,
                'qty' => $arg4,
                'hit' => $arg5,
            ];

            switch ($troop['owner']) {
                case 1:
                    $type = "mine";
                    break;
                default:
                    $type = "enemy";
                    break;
            }
            $this->troops['mine'][$entityId] = $troop;
        }

        // arg1: player that send the bomb: 1 if it is you, -1 if it is your opponent
        // arg2: identifier of the factory from where the bomb is launched
        // arg3: identifier of the targeted factory if it's your bomb, -1 otherwise
        // arg4: remaining number of turns before the bomb explodes (positive integer) if that's your bomb, -1 otherwise
        // arg5: unused
        if ($entityType == "BOMB") {
            $this->bombs =  [
                'entityId' => $entityId,
                'sender' => $arg1,
                'launchedFactory' => $arg2,
                'target' => $arg3,
                'explodeTurn' => $arg4
            ];
        }
    }

    public function prepareDefend()
    {
        // MOVE TO DEFEND - BEFORE ATTACK //

        // Calculate cyborg for defend cell 
        $countAttackingCyborg = 0;
        if (isset($troops[$F['entityId']])) {
            $countAttackingCyborg = $troops[$F['entityId']]['qty'];
        }
    }

    public function attack($factory) {
        if ( $factory['cybNumber'] == 0) { return; }
 
        // Prepare attacks
        foreach ($factory['links'] as $factoryId => $distance) {
            // If exist load neutral factory
            if (isset($this->factories['neutral'][$factoryId])) 
                $linkFactory = $this->factories['neutral'][$factoryId];
            // If exist load enemy factory
            elseif (isset($this->factories['enemy'][$factoryId])) {
                $linkFactory = $this->factories['enemy'][$factoryId];
            // If factory mine => skip
            } else {
                continue;
            }

            if ($this->controlRulesAttackOnFactory($factory, $linkFactory)) {
                // Attack on neutral cell
                if ($linkFactory['owner'] == 0) {
                    // Attack on neutral cell
                    $minCountTroopsForCapture = $linkFactory['cybNumber']+1;
                    // Calculate remaining cyborg in factory
                    $factory['cybNumber'] -= $linkFactory['cybNumber']+1;
                } 
                // Attack on enemy
                else {
                    // Calculate minimal troops to capture factory => calculate increase troops -> actual_troops + (turn * production) + 1
                    $minCountTroopsForCapture = $this->calMinCountTroopsForCapture($linkFactory, $distance);
                    if (($factory['cybNumber']) > $minCountTroopsForCapture) {
                        // Calculate remaining cyborg in factory
                        $factory['cybNumber'] -= $minCountTroopsForCapture;
                    }
                    
                }
                $this->addNewTarget($factory['entityId'], $linkFactory['entityId'], $minCountTroopsForCapture);
            }
            // $this->errorLog($this->attacks);
            // die;
            // When isnt factory with production > 0 -> if capture all factory with some production
            // if ($linkFactory['owner'] < 1) {
            //     $enemyFactoryId = $enemyFactoryId;
            // }
        }
        if (!empty($this->attacks)) {
            $this->command .= implode(";", $this->attacks).";";
        }
    }

    public function addNewTarget($sourceFactory, $targetFactory, $troopCount)
    {
        $this->attacks[] = "MOVE " . implode(" ", [$sourceFactory, $targetFactory, $troopCount]);
    }

    public function controlRulesAttackOnFactory($sourceFactory, $targetFactory)
    {
        if ($targetFactory['production'] == 0) {
            return false;
        }
        if ($sourceFactory['cybNumber'] < $targetFactory['cybNumber']) {
            return false;
        }
        return true;
    }


    public function calMinCountTroopsForCapture($enemyFactory, $distance)
    {
        return $enemyFactory['cybNumber'] + ($enemyFactory['production']*$distance) + 1;
    }

    public function prepareBomb($factory, $bomb, $factories) {
        if ( 
            $factory['owner'] == -1 &&
            $bomb['count'] < 2 &&
            $factory['production'] == 3
        ) {
            // Control if it isnt last attack destination
            if ($bomb['last_destination'] != $factory['entityId']) {
                $this->errorLog("Last-destination: ". $bomb['last_destination']);
                $this->errorLog("EntityId: ". $factory['entityId']);
                $bomb['destination'] = $factory['entityId'];

                // Find nearest my factory for send bomb
                foreach($factory['links'] as $key => $value) {
                    if ($factories[$key]['owner'] == 1){
                        $bomb['source'] = $key;
                        $bomb['sending'] = true;
                        break;
                    }
                }
            }
        }
        return $bomb;
    }

    public function startTimer()
    {
        $this->time = -microtime(true);
    }

    public function showTime()
    {
        $this->time += microtime(true);
        // Convert time to ms
        $this->time = $this->time*1000;
        $this->errorLog("TIME:" .round($this->time) . "ms");
    }

    public function errorLog($var)
    {
        error_log(var_export($var, true));   
    }
}