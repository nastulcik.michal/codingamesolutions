<?php
// $factoryCount: the number of factories
fscanf(STDIN, "%d", $factoryCount);
// $linkCount: the number of links between factories
fscanf(STDIN, "%d", $linkCount);

// BERE V POTAZ 
// POSILA VZDY NA NEJBLIZSI BUNKU KTERA MA VETSI PRODUKCI NEZ 1 
// POKUD TAKOVA BUNKA NENI POSILA NA POSLEDNI BEZ PRODUKCE
// POSILA BOMBY NA NEPRATELSKE BUNKY KTERE MAJI PRODUKCI 3
// POSILAT Z VICE BUNEK V JEDNOM TAHU
// POSILAT NA VICE BUNEK POKUD JE BUDE MOCT OBSADIT - KONTROLA POCTU POSLANYCH VOJAKU A ZBYVAJICICH
// BRAT V POTAZ UTOKY A ZPROSTRETKOVAT OCHRANU BUNEK - NEPOSILAT Z BUNKY KTERA JE NAPADENA VICE JEDNOTEK NEZ
//  NA NÍ ÚTOČÍ

// VYLEPSENI 
// UDELAT NEJAKE PRAVIDLO PRO CO NEJLEPSI POMER CENA/VYKON - VZDALENOST, POCET VOJAKU, PRODUKCE
// BRAT V POTAZ UTOKY A ZPROSTRETKOVAT OCHRANU BUNEK, NEJLEPE PODLE PRAVIDLA CENA/VYKON

// Collect factory links
for ($i = 0; $i < $linkCount; $i++)
{
    fscanf(STDIN, "%d %d %d", $factory1, $factory2, $distance);
    $factoryLinks[$factory1][$factory2] = $distance;
    $factoryLinks[$factory2][$factory1] = $distance;
}

// Sort links by distance
foreach($factoryLinks as $key => $value){
    asort($factoryLinks[$key]);
}

$bomb = [
    'send_troop_after_bomb' => false,
    'destination' => false,
    'sending' => false,
    'count' => 0,
];

while (TRUE)
{
    // Control if already conquere all factory 
    $existEnemyFactory = false;
    $attacks = [];
    $troops = [];

    // $entityCount: the number of entities (e.g. factories and troops)
    fscanf(STDIN, "%d", $entityCount);
    
    // Collect all data
    for ($i = 0; $i < $entityCount; $i++) {
        fscanf(STDIN,
            "%d %s %d %d %d %d %d", 
            $entityId, $entityType, $arg1, $arg2, $arg3, $arg4, $arg5
        );

        if ($entityType == "FACTORY") {
            $factories[$entityId] = [
                'entityId' => $entityId,
                'owner' => $arg1,
                'cybNumber' => $arg2,
                'production' => $arg3,
                'arg4' => $arg4,
                'arg5' => $arg5,
                'links' => $factoryLinks[$entityId],
            ];
        } else {
            // Only enemy troops 
            if ($arg1 == -1) {
                if (isset($troops[$arg3])){
                    $troops[$arg3]['qty'] += $arg4;
                }
                else {
                    $troops[$arg3] = [
                        'entityId' => $entityId,
                        'owner' => $arg1,
                        'source' => $arg2,
                        'destination' => $arg3,
                        'qty' => $arg4,
                        'hit' => $arg5,
                    ];
                }
            }
        }
    }

    // Prepare data - attack, bomb, defend
    for($entityId = 0; $entityId < count($factories); $entityId++) {
        $F = $factories[$entityId];

        // Prepare bomb
        if ( $F['owner'] == -1 && $bomb['count'] < 3) {
            if ($F['production'] == 3) {
                // Control if it isnt last attack destination
                if ($bomb['destination'] != $F['entityId']){
                    $bomb['destination'] = $F['entityId'];
                    foreach($F['links'] as $key => $value) {
                        if ($factories[$key]['owner'] == 1){
                            $bomb['source'] = $key;
                            $bomb['sending'] = true;
                            break;
                        }
                    }
                }
            }
        }

        // Prepare attacks
        if ( 
            $F['owner'] > 0 &&
            $F['cybNumber'] > 0)
        {
            $targets = [];

            // Calculate cyborg for defend cell 
            $countAttackingCyborg = 0;
            if (isset($troops[$F['entityId']])) {
                $countAttackingCyborg = $troops[$F['entityId']]['qty'];
            }

            // error_log(var_export("countAttackingCyborg::". $countAttackingCyborg, true));
            // error_log(var_export($F, true));

            $F['cybNumber'] -= $countAttackingCyborg;
            

            // Prepare attacks
            foreach ($F['links'] as $enemyFactoryId => $distance){
                $linkFactory = $factories[$enemyFactoryId];
                if ($linkFactory['production'] > 0 
                    && $linkFactory['owner'] < 1
                    && ($F['cybNumber']) > $linkFactory['cybNumber']
                    // && ($F['cybNumber'] > $countAttackingCyborg)
                    // && (!isset($troops[$linkFactory['entityId']]))
                    ) {
                    

                    // Attack on enemy - calculate with production increase 
                    if ($linkFactory['owner'] == -1)
                    {
                        $countCyborgForCapture = ($linkFactory['cybNumber'] + ( $linkFactory['production']*$distance))+1;
                        if (($F['cybNumber']) > $countCyborgForCapture) {
                            $sendCyborg = $countCyborgForCapture;
                            // Calculate remaining cyborg in factory
                            $F['cybNumber'] -= $countCyborgForCapture;
                            $targets[] = "MOVE ". $entityId . " ". $enemyFactoryId . " ". $countCyborgForCapture;
                        }
                    } 

                    // Attack on neutral cell
                    if ($linkFactory['owner'] < 1) {
                        $countCyborgForCapture = $linkFactory['cybNumber']+1;
                        // Calculate remaining cyborg in factory
                        $F['cybNumber'] -= $countCyborgForCapture;
                        $targets[] = "MOVE ". $entityId . " ". $enemyFactoryId . " ". $countCyborgForCapture;
                    }
                }

                // When isnt factory with production > 0 -> if capture all factory with some production
                if ($linkFactory['owner'] < 1) {
                    $enemyFactoryId = $enemyFactoryId;
                }
            }
            
            if (!empty($targets)){
                $attacks[] = implode(";", $targets);
            }
        }

        // Control if exist enemy factory
        if ($F['owner'] < 1) {
            $existEnemyFactory = true;
        }
    }

    $command = "WAIT";
    // Send troop after bomb
    if ($bomb['send_troop_after_bomb']){
        $attacks[] = $bomb['send_troop_after_bomb'];
        $bomb['send_troop_after_bomb'] = false;
    }

    if (!empty($attacks) && $existEnemyFactory) {
        // error_log(var_export($attacks, true));
        $command = implode(";", array_filter($attacks));
    }
    if ($bomb['sending']) {
        // error_log(var_export("BOMB", true));
        // error_log(var_export($bomb, true));
        $command .= ";BOMB " . $bomb['source'] . " ". $bomb['destination'];
        // Send one troop after bomb
        $bomb['send_troop_after_bomb'] .= "MOVE " . $bomb['source'] . " ". $bomb['destination'] . " 1";
        $bomb['sending'] = false;
        $bomb['count']++;
    }
    error_log(var_export($command, true));
    echo $command."\n";
}
?>