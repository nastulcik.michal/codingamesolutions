<?php
$magic_phrase = stream_get_line(STDIN, 500 + 1, "\n");

$class = new CodeOfTheRings($magic_phrase);
echo ($class->getActionSeq(). "\n");

class CodeOfTheRings
{
    const MOVE_RIGHT = '>';
    const MOVE_LEFT = '<';
    const ROLL_RUNE_FORWARD = '+';
    const ROLL_RUNE_BACKWARD = '-';
    const CONFIRM_RUNE = '.';
    const ZONE_COUNT = 30;
    const CHARACTER_COUNT = 27;

    private $zones;
    private $characters;
    private $bilbo_location = 0;

    public function __construct(string $magic_phrase) {
        $this->magic_phrase = $magic_phrase;
        // 30 zones = 30 elements. Default value is 1 - it is index for ' ' - space
        $this->zones = array_fill(0, self::ZONE_COUNT, 1);
        // Two empty values - symbol for space - are there for indexing character array from 1 no 0
        $this->characters = array_flip(array_merge([' ',' '], range('A', 'Z')));
    }

    public function getActionSeq()
    {
        $seq = '';
        for ($i=0; $i < strlen($this->magic_phrase); $i++) { 
            $char = $this->magic_phrase[$i];

            $seq .= $this->getActionSeqForGetChar($char);
        }
        $this->loger("Seq:". $seq);
        return $seq;
    }

    private function getActionSeqForGetChar(string $char)
    {
        $seq = '';
        $this->loger("char:". $char);

        $this->loger("char index:". $this->characters[$char]);
        $rune_index = $this->zones[0];

        $move = $this->getRuneMove($char, $rune_index);
        $seq = $this->getMoveSeq($move);
        
        $this->zones[0] = $this->characters[$char];
        $seq .= self::CONFIRM_RUNE;

        $this->loger("rune move direction val:" . $move);
        return $seq;
    }

    private function getRuneMove(string $char, int $rune_index): int
    {
        $char_index = $this->characters[$char];

        // Same char like actual
        if ($rune_index == $char_index) { return  0; }
        $move_backward = $this->getRuneMoveBackward($char_index, $rune_index);
        $move_forward = $this->getRuneMoveForward($char_index, $rune_index);
        
        return ($move_forward < $move_backward) ? $move_forward : $move_backward*(-1);
    }

    private function getRuneMoveBackward(int $char_index, int $rune_index): int
    {
        if ($rune_index > $char_index) { return $rune_index - $char_index; }
        return $rune_index + (self::CHARACTER_COUNT - $char_index);
    }

    private function getRuneMoveForward(int $char_index, int $rune_index): int
    {
        if ($char_index > $rune_index) { return $char_index - $rune_index; }
        return (self::CHARACTER_COUNT - $rune_index) + $char_index;
    }

    private function getMoveSeq(int $move): string
    {
        if ($move > 0) { return str_repeat(self::ROLL_RUNE_FORWARD, $move); }
        return str_repeat(self::ROLL_RUNE_BACKWARD, abs($move));
    }

    private function loger($var) {
        error_log(var_export($var, true));
    }
}
