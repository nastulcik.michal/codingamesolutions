<?php

$operation = stream_get_line(STDIN, 256 + 1, "\n");
fscanf(STDIN, "%d", $pseudoRandomNumber);
$rotors = [];
for ($i = 0; $i < 3; $i++)
{
    $rotors[] = stream_get_line(STDIN, 27 + 1, "\n");
}
$message = stream_get_line(STDIN, 1024 + 1, "\n");

$enigma_machine = new EnigmaMachine($pseudoRandomNumber, $rotors);
echo (($operation === "ENCODE") ? $enigma_machine->encodeMsg($message) : $enigma_machine->decodeMsg($message)) . "\n";

class EnigmaMachine {
    
    CONST ALPHABET = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    CONST ALPHABET_BY_KEY = ['A'=>0,'B'=>1,'C'=>2,'D'=>3,'E'=>4,'F'=>5,'G'=>6,'H'=>7,'I'=>8,'J'=>9,'K'=>10,'L'=>11,'M'=>12,'N'=>13,'O'=>14,'P'=>15,'Q'=>16,'R'=>17,'S'=>18,'T'=>19,'U'=>20,'V'=>21,'W'=>22,'X'=>23,'Y'=>24,'Z'=>25];

    public $rotors;

    function __construct(int $random_number, array $rotors) {
        $this->random_number = $random_number;
        $this->rotors = $rotors;
    }

    public function encodeMsg(string $msg) :string
    {
        return $this->applyEncodeRotors($this->applyCeasarShift($msg));
    }

    public function decodeMsg(string $msg) :string
    {
        return $this->applyCeasarShift( $this->applyDecodeRotors($msg), true);
    }

    private function applyCeasarShift(string $msg, bool $reverse = false) :string
    {
        $new_msg = '';
        for ($i=0; $i < strlen($msg); $i++) {
            $new_msg .= ($reverse) ? $this->ceasarUnshift($msg[$i], $this->random_number+$i) : $this->ceasarShift($msg[$i], $this->random_number+$i) ;
        }
        return $new_msg;
    }

    private function ceasarUnshift(string $char, int $decrement) : string
    {
        $alphabet_char_pos = self::ALPHABET_BY_KEY[$char];
        $new_pos = $this->upner($alphabet_char_pos - $decrement);
        return self::ALPHABET[$new_pos];
    }

    private function ceasarShift(string $char, int $increment) : string
    {
        $alphabet_char_pos = self::ALPHABET_BY_KEY[$char];
        $new_pos = $this->downer($alphabet_char_pos + $increment);
        return self::ALPHABET[$new_pos];
    }
    
    private function upner(int $pos) : int
    {
        if ($pos < 0) {
            return $this->upner($pos+26);
        }
        return $pos;
    }

    private function downer(int $pos) : int
    {
        if ($pos > 25) {
            return $this->downer($pos-26);
        }
        return $pos;
    }

    function applyEncodeRotors(string $msg) : string
    {
        foreach ( $this->rotors as $rotor) {
            $msg = $this->encodeRotor($rotor, $msg);
        }
        return $msg;
    }
    
    function applyDecodeRotors(string $msg) : string
    {
        foreach ( array_reverse($this->rotors) as $rotor) {
            $msg = $this->decodeRotor($rotor, $msg);
        }
        return $msg;
    }
    
    private function encodeRotor(string $rotor, string $msg) :string
    {
        $encode_msg = '';
        for ($i=0; $i < strlen($msg); $i++) {
            $alpha_index = self::ALPHABET_BY_KEY[$msg[$i]];
            $encode_msg .= $rotor[$alpha_index];
        }
        return $encode_msg;
    }
    
    private function decodeRotor(string $rotor, string $msg) : string
    {
        $decode_msg = '';
        // Example: "EKMFLGDQVZNTOWYHXUSPAIBRCJ" convert to => ['E' => 0, 'K' => 1 , 'M' => 2 ....]
        $rotor_arr = array_flip(str_split($rotor));
        for ($i=0; $i < strlen($msg); $i++) {
            $alpha_index = $rotor_arr[$msg[$i]];
            $decode_msg .= self::ALPHABET[$alpha_index];
        }
        return $decode_msg;
    }
}
