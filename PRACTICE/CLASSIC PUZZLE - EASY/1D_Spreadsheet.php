<?php
fscanf(STDIN, "%d", $N);
$spreadSheet = [];

for ($i = 0; $i < $N; $i++)
{
    fscanf(STDIN, "%s %s %s", $operation, $arg1, $arg2);

    $spreadSheet[] = [
        'operation' => $operation,
        'arg1' => $arg1,
        'arg2' => $arg2,
    ];
}

$c = new CalculateSpreadSheetCells($spreadSheet);
$c->calculateCells();
$c->echoCells();

class CalculateSpreadSheetCells
{   
    private $cellResults = [];

    private $spreadSheet = [];

    function __construct($spreadSheet) {
        $this->spreadSheet = $spreadSheet;
    }

    public function calculateCells() : void
    {
        foreach ($this->spreadSheet as $cellNumber => $cell) {
            // Control if cell already calculate - recursive by reference
            if (isset($this->cellResults[$cellNumber]) === false ) {
                $this->cellResults[$cellNumber] = $this->calculateCell($cell);
            }
        }
        // Must sort by cell numbers - can be insert to array in invalid order - recursion by reference
        ksort($this->cellResults);
    }

    public function echoCells(): void 
    {
        foreach ($this->cellResults as $cellNumber => $cell) {
            echo $cell."\n";
        }
    }

    private function calculateCell(array $cell): int
    {
        $arg1 = $this->getCellArgumentValue($cell['arg1']);
        $arg2 = $this->getCellArgumentValue($cell['arg2']);

        switch ($cell['operation']) {
            case  'VALUE': return $arg1;
            case  'ADD': return $arg1 + $arg2;
            case  'SUB': return $arg1 - $arg2;
            case  'MULT': return $arg1 * $arg2;
        }
    }

    private function getCellArgumentValue(string $arg) : int
    {
        if ($this->isNotReference($arg)) { 
            return (int)$arg;
        }

        $referenceIndex = $this->getReferenceIndexFromArgument($arg);

        if (isset($this->cellResults[$referenceIndex])) {
            return $this->cellResults[$referenceIndex];
        } 
        
        $referenceCellValue = (int)$this->calculateCell($this->spreadSheet[$referenceIndex]);
        $this->cellResults[$referenceIndex] = $referenceCellValue;
        return $referenceCellValue;
    }
    
    private function isNotReference($arg) : bool
    {
        return ($arg[0] !== "$");
    }

    function getReferenceIndexFromArgument($arg): int
    {
        return (int)ltrim($arg, "$");
    }
}