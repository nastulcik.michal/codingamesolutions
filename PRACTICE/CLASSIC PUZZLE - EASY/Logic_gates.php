<?php
fscanf(STDIN, "%d", $n);
fscanf(STDIN, "%d", $m);

CONST ONE = '-';
CONST ZERO = '_';

$inputSignals = [];
$outputs = [];

for ($i = 0; $i < $n; $i++)
{
    fscanf(STDIN, "%s %s", $inputName, $inputSignal);
    
    $inputSignals[$inputName] = $inputSignal;
}

for ($i = 0; $i < $m; $i++)
{
    fscanf(STDIN, "%s %s %s %s", $outputSignalName, $type, $inputName1, $inputName2);
    $gatesData[$outputSignalName] = [
        'type' => $type,
        'input_name_1' => $inputName1,
        'input_name_2' => $inputName2,
    ];
}

foreach ( $gatesData as $outputSignalName => $gateData) {
    $outputSignal = processSignalThroughGate(
        $gateData['type'],
        $inputSignals[$gateData['input_name_1']],
        $inputSignals[$gateData['input_name_2']]
    );

    echo $outputSignalName . " " . $outputSignal . "\n";
}

function processSignalThroughGate(string $gateType, string $inputSignalOne, string $inputSignalTwo): string
{
    $processedSignal = '';

    for ($i = 0; $i < strlen($inputSignalOne); $i++) {
        $signalOne = $inputSignalOne[$i];
        $signalTwo = $inputSignalTwo[$i];
        
        switch($gateType) {
            case 'AND':
                $processedSignal .= ($signalOne === $signalTwo) ? $signalOne : ZERO;
                break;
            case 'OR':
                $processedSignal .= ($signalOne === ZERO && $signalTwo === ZERO) ? ZERO : ONE;
                break;
            case 'XOR':
                $processedSignal .= ($signalOne === $signalTwo) ? ZERO : ONE;
                break;
            case 'NAND':
                $processedSignal .= ($signalOne === ONE && $signalTwo === ONE) ? ZERO : ONE;
                break;
            case 'NOR':
                $processedSignal .= ($signalOne === ZERO && $signalTwo === ZERO) ? ONE : ZERO;
                break;
            case 'NXOR':
                $processedSignal .= ($signalOne === $signalTwo) ? ONE : ZERO;
                break;
        }
        
    }
    return $processedSignal;
}
