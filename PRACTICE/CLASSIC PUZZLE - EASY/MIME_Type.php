<?php
// $N: Number of elements which make up the association table. // $Q: Number Q of file names to be analyzed.
fscanf(STDIN, "%d", $N); fscanf(STDIN, "%d", $Q);

for ($i = 0; $i < $N; $i++)
{
    // $EXT: file extension // $MT: MIME type.
    fscanf(STDIN, "%s %s", $EXT, $MT);
    $filesMIME[strtolower($EXT)] = $MT;
}
$filesMIME["unknown"] = "UNKNOWN";

for ($i = 0; $i < $Q; $i++)
{
    $FNAME = stream_get_line(STDIN, 256 + 1, "\n"); // One file name per line.
    $mimeType = "unknown";

    if (((strpos($FNAME, ".")) !== FALSE)) { 
        $e = explode(".", strtolower($FNAME));
        $extension = end($e);
        if (isset($filesMIME[$extension])){
            $mimeType = $extension;
        }
    }
    echo($filesMIME[$mimeType]."\n");
}