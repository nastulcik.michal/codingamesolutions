<?php
/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

fscanf(STDIN, "%d %d %d", $h, $w, $n);
$live = stream_get_line(STDIN, 9 + 1, "\n");
$birth = stream_get_line(STDIN, 9 + 1, "\n");

$grid = [];

for ($i = 0; $i < $h; $i++)
{
    $grid[] = stream_get_line(STDIN, $w + 1, "\n");
}

$game = new GameOfLive($grid, $h, $w, $live, $birth);
$game->simulate($n);
$game->echoOutput($n);

class GameOfLive
{
    // Grid with cells - board for game of live
    private $grid = [];

    // Live conditions
    private $live_con;

    // Birth conditions
    private $birth_con;

    // Show cells grid beetween turns with number of neighbours
    private $debug = true;

    public function __construct(
        array $start_grid,
        int $grid_height,
        int $grid_width,
        string $live_con,
        string $birth_con
    )
    {
        $this->grid = $start_grid;
        $this->grid_height = $grid_height;
        $this->grid_width = $grid_width;
        $this->live_con = $live_con;
        $this->birth_con = $birth_con;
    }

    public function simulate(int $turns)
    {
        for ($i=0; $i < $turns; $i++) { 
            $this->simulateTurn();
        }
    }

    public function echoOutput()
    {
        foreach ($this->grid as $row) {
            echo $row."\n";
        }
    }

    private function simulateTurn()
    {
        $new_grid = [];

        foreach ($this->grid as $row_index => $row) {
            $new_grid[$row_index] = ''; 
            for ($cell_index=0; $cell_index < strlen($row); $cell_index++) {
                $cell_neigh = $this->getCountOfNeighbours($row_index, $cell_index);
                $cell_val = $this->grid[$row_index][$cell_index];

                if ($this->isCellLive($cell_val)) {
                    $new_grid[$row_index] .= ($this->cellSurvive($cell_neigh)) ? "O" : '.';
                } else {
                    $new_grid[$row_index] .= ($this->birthCell($cell_neigh)) ? "O" : '.';
                }
            }
        }
        $this->grid = $new_grid;
    }

    private function getCountOfNeighbours(int $row_index, int $cell_index): int
    {
        $n = 0;

        // TOP ROW
        $n += $this->getCountLiveCellOnRow($row_index-1, $cell_index);

        // MIDDLE ROW
        $n += $this->getCountLiveCellOnRow($row_index, $cell_index, false);

        // BOTTOM ROW
        $n += $this->getCountLiveCellOnRow($row_index+1, $cell_index);

        return $n;
    }

    private function getCountLiveCellOnRow(int $row_index, int $cell_index, bool $count_middle = true): int
    {
        $c = 0;

        if (isset($this->grid[$row_index]) === false) { 
            return $c;
        }

        $r = $this->grid[$row_index];

        if ($count_middle) {
             // middle cell
            $c += (($this->isCellLive($r[$cell_index])) ? 1 : 0);
        }
        
        // right cell
        if (isset($r[$cell_index+1])) {
            $c += (($this->isCellLive($r[$cell_index+1])) ? 1 : 0);
        }

        // left cell
        if (isset($r[$cell_index-1]) && (($cell_index -1) >= 0)) {
            $c += (($this->isCellLive($r[$cell_index-1])) ? 1 : 0);
        }

        return $c;
    }

    private function isCellLive(string $cell): bool
    {
        return ($cell === "O") ? true : false;
    }

    private function cellSurvive(int $neigh): bool
    {
        return ($this->live_con[$neigh]) ? true : false;
    }

    private function birthCell(int $neigh): bool
    {
        return ($this->birth_con[$neigh]) ? true : false;
    }

    private function log($var): void
    {
        error_log(var_export($var, true));
    }
}
