<?php
$subgridLevel = 0;
for ($i = 0; $i < 9; $i++)
{
    $row = array_flip(explode(" ", trim(fgets(STDIN))));
    // Control row
    controlIfContainRequiredDigit([$row]);

    if ($i % 3 == 0) { $subgridLevel++; }
    $c = 0 + $subgridLevel;
    foreach($row as $digit => $key)
    {
        if ($key % 3 == 0){ $c++; }
        $columns[$key][$digit] = $digit;
        $subgrids[$c][$digit] = $digit;
    }
}
// Control columns
controlIfContainRequiredDigit($columns);
// Control subgrids
controlIfContainRequiredDigit($subgrids);
echo("true\n");

function controlIfContainRequiredDigit($multiArray) {
    foreach ($multiArray as $array) {
        if (count($array) != 9)
            exit("false\n");
    }
}
