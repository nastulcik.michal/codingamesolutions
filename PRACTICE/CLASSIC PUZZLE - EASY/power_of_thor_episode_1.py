import sys
import math
light_x, light_y, thor_x, thor_y = [int(i) for i in input().split()]

while True:
    remaining_turns = int(input())  # The remaining amount of turns Thor can move. Do not remove this line.
    # To debug: print("Debug messages...", file=sys.stderr, flush=True)
    # A single line providing the move to be made: N NE E SE S SW W or NW
    direction = ''
    if thor_y > light_y:
        direction += "N"
        thor_y-=1
    if thor_y < light_y:
        direction += "S"
        thor_y+=1
    if thor_x > light_x:
        direction += "W"
        thor_x-=1
    if thor_x < light_x:
        direction += "E"
        thor_x+=1
    print(direction)
