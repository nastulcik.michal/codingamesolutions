n = int(input())
strengths = []
for i in range(n):
    strengths.append(int(input()))

strengths.sort()

closestStrength = 1000000
for i in range(1,len(strengths)):
    diff = abs(strengths[i-1] - strengths[i])
    if ( diff  < closestStrength):
        closestStrength = diff    
print(closestStrength)
