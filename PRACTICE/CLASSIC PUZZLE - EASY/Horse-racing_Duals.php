<?php
fscanf(STDIN, "%d", $N);
$strengths = [];

for ($i = 0; $i < $N; $i++)
{
    fscanf(STDIN, "%d", $pi);
    $strengths[] = $pi;
}

arsort($strengths);
$strengths = array_values($strengths);
$closest = 99999999999;

foreach($strengths as $key => $val) {
    if ($key > 0) {
        if (($strengths[$key-1] - $val) <= $closest) {
            $closest = $strengths[$key-1] - $val;
        } 
    }
}
// To debug: error_log(var_export($var, true)); (equivalent to var_dump)
echo("$closest\n");