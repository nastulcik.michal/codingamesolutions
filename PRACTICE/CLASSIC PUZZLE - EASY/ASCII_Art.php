<?php
/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
fscanf(STDIN, "%d", $L);
fscanf(STDIN, "%d", $H);
$T = stream_get_line(STDIN, 256 + 1, "\n");

// Auxiliary variables
$ASCIIchars = [];
$alphas = array_flip(range('A', 'Z'));
$char = "";

// Save ASCII characters to array
for ($i = 0; $i < $H; $i++)
{
    $ROW = stream_get_line(STDIN, 1024 + 2, "\n");
    $charCounter = 0;
    
    for($x = 0; $x < strlen($ROW); $x++){
        $char .= $ROW[$x]; 
        
        // Control character end
        if ((($x+1) % $L) == 0) {
            $ASCIIchars[$charCounter][$i] = $char;
            $charCounter++;
            $char = "";
        }
    }
}

// Create ASCII string by $T
$asciiArt = "";
for($i = 0; $i < $H; $i++) {
    for($z = 0; $z < strlen($T); $z++) {
        $upperChar = strtoupper($T[$z]);
        if (isset($alphas[$upperChar])) {
            $asciiArt .= $ASCIIchars[$alphas[$upperChar]][$i];
        } else {
            // Question mark
            $asciiArt .= $ASCIIchars[26][$i];
        }
    }
    $asciiArt .= "\n";
}

// Write an action using echo(). DON'T FORGET THE TRAILING \n
// To debug: error_log(var_export($var, true)); (equivalent to var_dump)
echo("$asciiArt\n");