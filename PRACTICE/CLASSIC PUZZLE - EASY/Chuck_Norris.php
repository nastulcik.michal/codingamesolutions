<?php
$MESSAGE = stream_get_line(STDIN, 100 + 1, "\n");
$strBinary = "";
$unaryMessage = "";
$unary = 1;

for ($i = 0; $i < strlen($MESSAGE); $i++) {
    $charBinary = decbin(ord($MESSAGE[$i]));
    if (strlen($charBinary) != 7){ 
      $charBinary = str_repeat('0', 7-strlen($charBinary)) . $charBinary;
    }
    $strBinary .= $charBinary;
}

for ($x = 0; $x < strlen($strBinary); $x++) {
    if (isset($strBinary[$x+1]) && ( $strBinary[$x] == $strBinary[$x+1] )) {
        $unary++; continue;
    } 
    $unaryMessage .= (($strBinary[$x]) ? "0 " : "00 ") . str_repeat('0', $unary) . " ";
    $unary = 1;
}

$unaryMessage = trim($unaryMessage);
echo("$unaryMessage\n");