<?php

class LogicGates
{
    protected CONST ONE = '-';
    protected CONST ZERO = '_';

    public function processTheSignals(string $gateType, string $inputSignalOne, string $inputSignalTwo): string
    {
        $processedSignal = '';
        $methodName = "logicGate".$gateType;

        for ($i = 0; $i < strlen($inputSignalOne); $i++) {
            $signalCharacter1 = $inputSignalOne[$i];
            $signalCharacter2 = $inputSignalTwo[$i];
            $processedSignal .= $this->$methodName($signalCharacter1, $signalCharacter2);
        }
        
        return $processedSignal;
    
    }

    private function logicGateAND(string $signalCharacter1, string $signalCharacter2): string
    {
        return ($signalCharacter1 === $signalCharacter2) ? $signalCharacter1 : self::ZERO;
    }

    private function logicGateOR(string $signalCharacter1, string $signalCharacter2): string
    {
        return ($signalCharacter1 === self::ZERO && $signalCharacter2 == self::ZERO) ? self::ZERO : self::ONE;
    }

    private function logicGateXOR(string $signalCharacter1, string $signalCharacter2): string
    {
        return ($signalCharacter1 === $signalCharacter2) ? self::ZERO : self::ONE;
    }

    private function logicGateNAND(string $signalCharacter1, string $signalCharacter2): string
    {
        return ($signalCharacter1 === self::ONE && $signalCharacter2 === self::ONE) ? self::ZERO : self::ONE;
    }

    private function logicGateNOR(string $signalCharacter1, string $signalCharacter2): string
    {
        return ($signalCharacter1 === self::ZERO && $signalCharacter2 === self::ZERO) ? self::ONE : self::ZERO;
    }

    private function logicGateNXOR(string $signalCharacter1, string $signalCharacter2): string
    {
        return ($signalCharacter1 === $signalCharacter2) ? self::ONE : self::ZERO;
    }
}

fscanf(STDIN, "%d", $n);
fscanf(STDIN, "%d", $m);

$inputSignals = [];
$gatesData = [];

for ($i = 0; $i < $n; $i++)
{
    fscanf(STDIN, "%s %s", $inputName, $inputSignal);
    
    $inputSignals[$inputName] = $inputSignal;
}

for ($i = 0; $i < $m; $i++)
{
    fscanf(STDIN, "%s %s %s %s", $outputSignalName, $type, $inputSignalName1, $inputSignalName2);
    $gatesData[$outputSignalName] = [
        'type' => $type,
        'input_signal_name_1' => $inputSignalName1,
        'input_signal_name_2' => $inputSignalName2,
    ];
}

$logicGates = new LogicGates();

foreach ( $gatesData as $outputSignalName => $gateData) {
    $outputSignal = $logicGates->processTheSignals(
        $gateData['type'],
        $inputSignals[$gateData['input_signal_name_1']],
        $inputSignals[$gateData['input_signal_name_2']]
    );

    echo $outputSignalName . " " . $outputSignal . "\n";
}
