<?php
// https://www.codingame.com/training/easy/asteroids

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

fscanf(STDIN, "%d %d %d %d %d", $W, $H, $T1, $T2, $T3);

$pictures = [];
$pictures['height'] = $H;
$pictures['weight'] = $W;
$times = [
    "T1" => $T1,
    "T2" => $T2,
    "T3" => $T3
];

for ($i = 0; $i < $H; $i++)
{
    fscanf(STDIN, "%s %s", $firstPictureRow, $secondPictureRow);
    $pictures['first'][] = $firstPictureRow;
    $pictures['second'][] = $secondPictureRow;
}

$asteroids = new Asteroids($pictures, $times);
// debug($asteroids->pictures['third']);

foreach ($asteroids->pictures['third'] as $rowId => $row) {
    echo $row . "\n";
}

class Asteroids {
    public $pictures;
    protected $coordinations;
    protected $times;
    protected $alphabet;

    public function __construct($pictures, $times) {
        $this->pictures = $pictures; 
        $this->times = $times;
        $this->setAlphabet();
        $this->setAstrCoordinationsByPictures();
        $this->prepareThirdPicture($this->calculateNextAstrPositions());
    }
    
    public function prepareThirdPicture($asteroidPositions) {
        $pictureThird = [];
        $position = 0;
        for ($i = 0; $i < $this->pictures['height']; $i++) {
            for ($x = 0; $x < $this->pictures['weight']; $x++) {
                $pictureThird[$i] .= (isset($asteroidPositions[$position])) ? $asteroidPositions[$position] : ".";
                $position++;
            }
        }
        $this->pictures['third'] = $pictureThird;
    }

    private function setAlphabet(){
        $this->alphabet = array_flip(range('A', 'Z'));
    }

    private function setAstrCoordinationsByPictures() {
        $this->coordinations['picture_first'] = $this->getAstrCoordinationsByPicture($this->pictures['first']);
        $this->coordinations['picture_second'] = $this->getAstrCoordinationsByPicture($this->pictures['second']);
    }

    private function getAstrCoordinationsByPicture($picture) {
        $astCoordinates = [];

        foreach( $picture as $rowId => $row) {
            $splitRow = str_split($row);
            foreach($splitRow as $columnId => $asteroidIndex) {
                if ($asteroidIndex != '.') {
                    $astCoordinates[$asteroidIndex] = [
                        'x' => $columnId,
                        'y' => $rowId,
                    ];
                }
            }
        }
        return $astCoordinates;
    }

    private function calculateNextAstrPositions() {
        $astrPositions = [];

        foreach($this->coordinations['picture_first'] as $asteroidChar => $coorPicOne) {
            $coorPicTwo = $this->coordinations['picture_second'][$asteroidChar];
            $nextAstrCoor = $this->getNextAsteroidCoorOnPicture($coorPicOne, $coorPicTwo, $asteroidChar);

            if ($this->isAsteroidInPictureBorder($nextAstrCoor)) {
                $pos = $this->convertCoorToPosOnPicture($nextAstrCoor);
                
                // Asteroids travel at different altitudes (with A being the closest and Z the farthest from your observation point) 
                // and therefore cannot collide with each other during their transit.
                // If two or more asteroids have the same final coordinates, output only the closest one.
                if (isset($astrPositions[$pos])) {
                    if ($this->alphabet[$asteroidChar] < $this->alphabet[$astrPositions[$pos]]) {
                        $astrPositions[$pos] = $asteroidChar;
                    }
                } else {
                    $astrPositions[$pos] = $asteroidChar;
                }
            }
        }
        return $astrPositions;
    }

    private function convertCoorToPosOnPicture($coor) {
        return ($this->pictures['height']* $coor['y']) + $coor['x'];
    }

    private function isAsteroidInPictureBorder($coor) {
        if (($coor['x'] >= $this->pictures['weight']) || ($coor['x'] < 0)) {
            return false;
        }
        if (($coor['y'] >= $this->pictures['height']) || ($coor['y'] < 0)) {
            return false;
        }
        return true;
    }

    private function getNextAsteroidCoorOnPicture($coor1, $coor2, $asteroidChar) {
        $timeCoef = $this->getTimeCoeficient();
        $moveX = floor($this->getAsteroidMove($coor1['x'], $coor2['x']) * $timeCoef);
        $moveY = floor($this->getAsteroidMove($coor1['y'], $coor2['y']) * $timeCoef);
        $newXpos = $coor2['x'] + $moveX;
        $newYpos = $coor2['y'] + $moveY;

        return [
            'x' => $newXpos,
            'y' => $newYpos,
        ];
    }

    private function getAsteroidMove($move1, $move2) {
        $moveDiff = abs($move1 - $move2);
        return ($move1 > $move2) ? -$moveDiff : $moveDiff;
    }

    private function getTimeCoeficient() {
        $timeDiff1 = $this->times['T2'] - $this->times['T1'];
        $timeDiff2 = $this->times['T3'] - $this->times['T2'];
        $timeDiff = $timeDiff2/$timeDiff1;
        return $timeDiff;
    }
}

function debug($var) {
    error_log(var_export($var, true));
}
