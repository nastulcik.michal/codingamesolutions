<?php
fscanf(STDIN, "%d %d %d %d %d %d %d %d", $nbFloors, $width, $nbRounds, $exitFloor, $exitPos, $nbTotalClones, $nbAdditionalElevators, $nbElevators);

for ($i = 0; $i < $nbElevators; $i++)
{
    fscanf(STDIN, "%d %d", $elevatorFloor, $elevatorPos);
    $floors[$elevatorFloor] = $elevatorPos;
}
$floors[$exitFloor] = $exitPos;

while (TRUE)
{
    fscanf(STDIN, "%d %d %s", $cloneFloor, $clonePos, $direction);
    $command = "WAIT";
    if ($floors[$cloneFloor] > $clonePos && $direction == "LEFT")
        $command = "BLOCK";
    if ($floors[$cloneFloor] < $clonePos && $direction == "RIGHT")
        $command = "BLOCK";
    echo $command. "\n";
}