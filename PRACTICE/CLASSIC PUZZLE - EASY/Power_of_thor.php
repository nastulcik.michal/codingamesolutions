<?php
// $lightX: the X position of the light of power
// $lightY: the Y position of the light of power
// $initialTx: Thor's starting X position
// $initialTy: Thor's starting Y position
fscanf(STDIN, "%d %d %d %d", $lightX, $lightY, $initialTx, $initialTy);

$thorY = $initialTy;
$thorX = $initialTx;

// game loop
while (TRUE)
{
    fscanf(STDIN, "%d", $remainingTurns);

    if ($thorY < $lightY ){
        echo("S");    
        $thorY++;
    }
    if ($thorX > $lightX){
        echo("W");    
        $thorX--;
    }
    if ($thorX < $lightX){
        echo("E");    
        $thorX++;
    }
    if ($thorY > $lightY){
        echo("N");
        $thorY--;
    }
    echo("\n");
}