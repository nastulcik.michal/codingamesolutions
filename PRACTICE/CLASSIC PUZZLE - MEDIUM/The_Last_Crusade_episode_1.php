<?php
// $W: number of columns.
// $H: number of rows.
fscanf(STDIN, "%d %d", $W, $H);

$grid = [];
$roomTypes = [
    1 => [ "TOP" => ['y' => 1], "LEFT" => ['y' => 1], "RIGHT" => ['y' => 1]],
    2 => [ "LEFT" => ['x' => 1], "RIGHT" => ['x' => -1]],
    3 => [ 'TOP' => ['y' => 1]],
    4 => [ 'RIGHT' => ['y' => 1], 'TOP' => ['x' => -1]],
    5 => [ 'TOP' => ['x' => 1], 'RIGHT' => ['x' => 1], 'LEFT' => ['y' => 1]],
    6 => [ 'RIGHT' => ['x' => -1], 'LEFT' => ['x' => 1]],
    7 => [ 'TOP' => ['y' => 1], 'RIGHT' => ['y' => 1]],
    8 => [ 'RIGHT' => ['y' => 1], 'LEFT' => ['y' => 1]],
    9 => [ 'TOP' => ['y' => 1], 'LEFT' => ['y' => 1]],
    10 => [  "TOP" => ['x' => -1]],
    11 => [  "TOP" => ['x' => 1]],
    12 => [ "RIGHT" => ['y' => 1]],
    13 => [ "LEFT" => ['y' => 1]],
];

for ($i = 0; $i < $H; $i++)
{
    $grid[$i] = explode(" ", stream_get_line(STDIN, 200 + 1, "\n"));
}

// $EX: the coordinate along the X axis of the exit (not useful for this first mission, but must be read).
fscanf(STDIN, "%d", $EX);

while (TRUE)
{
    fscanf(STDIN, "%d %d %s", $XI, $YI, $POS);
    $direction = $roomTypes[$grid[$YI][$XI]][$POS];
    if (isset($direction['y'])) {
        $nextMove = $XI. ' ' . ($YI + $direction['y']);
    } else {
        $nextMove = ($XI + $direction['x']) . ' ' . $YI;
    }
    echo("$nextMove\n");
}