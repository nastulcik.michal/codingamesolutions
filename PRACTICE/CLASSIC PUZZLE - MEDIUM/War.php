<?php
// $n: the number of cards for player 1
fscanf(STDIN, "%d", $n);

$player1Deck = [];
$player2Deck = [];

// Array for convert card char to int
$cardValues = ['J' => 11, 'Q' => 12, 'K' => 13, 'A' => 14];

for ($i = 0; $i < $n; $i++)
{
    // $cardp1: the n cards of player 1
    fscanf(STDIN, "%s", $cardp1);
    $cardValue = preg_replace("/[^0-9JQKA]/", '', $cardp1);

    if (isset($cardValues[$cardValue])) {
        $cardValue = $cardValues[$cardValue];
    }
    $player1Deck[] = (int)$cardValue;
}

// $m: the number of cards for player 2
fscanf(STDIN, "%d", $m);
for ($i = 0; $i < $m; $i++)
{
    // $cardp2: the m cards of player 2
    fscanf(STDIN, "%s", $cardp2);
    $cardValue = preg_replace("/[^0-9JQKA]/", '', $cardp2);
    if (isset($cardValues[$cardValue])) {
        $cardValue = $cardValues[$cardValue];
    }
    $player2Deck[] = (int)$cardValue;
}


$cardDecksWar = new CardDecksWar($player1Deck, $player2Deck);
echo $cardDecksWar->determineWinner() . "\n";

class CardDecksWar {

    const COUNT_CARD_FACE_DOWN_FOR_WAR = 3;

    private $player1CardDeck = [];

    private $player2CardDeck = [];

    private $roundCounter = 1;

    private $playerWinnerNumber = 0;

    private $pat = false;

    public function __construct(array $deckOne, array $deckTwo) {
        $this->player1CardDeck = $deckOne;
        $this->player2CardDeck = $deckTwo;
    }

    public function determineWinner(): string
    {
        $this->battle();

        if ($this->pat) {
            return "PAT";
        }
        return $this->playerWinnerNumber . " " . $this->roundCounter;
    }

    private function battle() : void
    {
        $deck1 = $this->player1CardDeck;
        $deck2 = $this->player2CardDeck;
        $pot1 = [];
        $pot2 = [];

        while(1) {
            $card1 = array_shift($deck1);
            $card2 = array_shift($deck2);

            if ($card1 !== $card2) {
                array_push($pot1, $card1);
                array_push($pot2, $card2);

                if ($card1 > $card2) {
                    $deck1 = array_merge($deck1, $pot1, $pot2);
                } else {
                    $deck2 = array_merge($deck2, $pot1, $pot2);
                }

                $pot1 = [];
                $pot2 = [];

                if (empty($deck1)) {
                    $this->playerWinnerNumber = 2;
                    break;
                }

                if (empty($deck2)) {
                    $this->playerWinnerNumber = 1;
                    break;
                }
                $this->roundCounter++;

            } else if (count($deck1) < 3 || count($deck2) < 3) {
                $this->pat = true;
                break;
            } else {
                array_push( $pot1, $card1);
                array_push( $pot2, $card2);
                
                $this->moveDeckCardToPot($pot1, $deck1, self::COUNT_CARD_FACE_DOWN_FOR_WAR);
                $this->moveDeckCardToPot($pot2, $deck2, self::COUNT_CARD_FACE_DOWN_FOR_WAR);
            }
        }
    }

    private function moveDeckCardToPot(array &$pot, array &$deck, int $cardCount) : void
    {
        for ($i=0; $i < $cardCount; $i++) {
            array_push( $pot, array_shift($deck));
        }
    }

    private function debug($var) {
        error_log(var_export($var, true));
    }
}
