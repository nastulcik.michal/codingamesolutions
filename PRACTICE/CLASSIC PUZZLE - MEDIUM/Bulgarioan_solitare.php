<?php
fscanf(STDIN, "%d", $N);
$inputs = explode(" ", fgets(STDIN));

for ($i = 0; $i < $N; $i++)
{
    $C = intval($inputs[$i]);
    if ($C > 0)
        $piles[] = $C;
}

$match = true;
$turns[implode("", $piles)] = 1;
$counter = 0;

while ($match){
    $newPiles = [];

    for ($i=0; $i < count($piles); $i++){
        if (($piles[$i]-1) > 0)
            $newPiles[] = $piles[$i]-1;
    }
    $newPiles[] = count($piles);
    asort($newPiles);
    $piles = $newPiles;
    $turn = implode("", $newPiles);

    if (isset($turns[$turn])) {
        $loop = $counter - $turns[$turn];
        $match = false;
    } else {
        $turns[$turn] = $counter++;
    }
}
// To debug: error_log(var_export($var, true)); (equivalent to var_dump)
echo($loop."\n");