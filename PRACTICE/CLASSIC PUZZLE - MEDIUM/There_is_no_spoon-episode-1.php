<?php
fscanf(STDIN, "%d", $width);
fscanf(STDIN, "%d", $height);

$grid = [];
for ($y = 0; $y < $height; $y++)
{
    $line = stream_get_line(STDIN, 31 + 1, "\n");// width characters, each either 0 or .
    for ($z=0; $z < strlen($line); $z++) {
        $grid[$y][] = ($line[$z] != ".") ? 'full' : 'empty';
    }
}

function controlRightNode($grid, $row, $nodeIndex) {
    $nodeIndex++;
    if (!isset($grid[$row][$nodeIndex])) { return false; }
    if ($grid[$row][$nodeIndex] != 'empty') { return $nodeIndex; }
    return controlRightNode($grid, $row, $nodeIndex);
}

function controlBottomNode($grid, $row, $nodeIndex) {
    $row++;
    if (!isset($grid[$row])) { return false;}
    if ($grid[$row][$nodeIndex] != 'empty') { return $row;}
    return controlBottomNode($grid, $row, $nodeIndex);
}

for ($y=0; $y < count($grid); $y++) {
    for ($x=0; $x < count($grid[$y]); $x++) {
        // Coordinate only full cell
        if ($grid[$y][$x] != "empty"){
            // Actual node coordinates
            $actual = $x." ".$y;

            // Control right node
            $coordinateX = (controlRightNode($grid, $y, $x));
            $right = ($coordinateX) ? $coordinateX." ".$y : "-1 -1";

            // Control bottom node
            $coordinateY = (controlBottomNode($grid, $y, $x));
            $bottom = ($coordinateY) ? $x." ".$coordinateY : '-1 -1';

            // Three coordinates: a node, its right neighbor, its bottom neighbor
            echo $actual ." ". $right." ". $bottom."\n";
        }
    }
}