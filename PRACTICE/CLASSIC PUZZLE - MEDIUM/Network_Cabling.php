<?php
fscanf(STDIN, "%d", $buildingNumber);

for ($i = 0; $i < $buildingNumber; $i++)
{
    fscanf(STDIN, "%d %d", $building_coordinate_x, $building_coordinate_y);
    $buildingsCoordinateY[] = $building_coordinate_y;
    $buildingsCoordinatesX[] = $building_coordinate_x;
}

function getLengthCableToBuildings(array $buildingsCoordinateY, int $coordinateYForMainCable) {
    $cableLength = 0;
    foreach ($buildingsCoordinateY as $coordinateY) {
        $cableLength += abs($coordinateY - $coordinateYForMainCable);
    }
    return $cableLength;
}

// Return median
function getBestCoordinateYForMainCable($buildingNumber, $buildingsCoordinateY) : int
{
    if ($buildingNumber % 2 !== 0) {
        return $buildingsCoordinateY[$buildingNumber/2];
    }
    return (($buildingsCoordinateY[$buildingNumber/2-1] + $buildingsCoordinateY[$buildingNumber/2]) / 2);
}

function getMainCableLength(array $buildingsCoordinatesX): int
{
    return max($buildingsCoordinatesX) - min($buildingsCoordinatesX);
}

sort($buildingsCoordinateY);
$coordinateYForMainCable = getBestCoordinateYForMainCable($buildingNumber, $buildingsCoordinateY);
$totalLength = getMainCableLength($buildingsCoordinatesX) + getLengthCableToBuildings($buildingsCoordinateY, $coordinateYForMainCable);

echo $totalLength . "\n";